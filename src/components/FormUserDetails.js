import { AppBar, Button, TextField } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { ThemeProvider } from "@material-ui/styles";
import React from "react";
import theme from "../theme";


const styles = {
	button: {
		margin: "15px",
	},
};

function FormUserDetails({ values:{firstName,lastName, email}, handleChange, nextStep }) {

	return (
		<ThemeProvider theme={theme}>

			<AppBar position="static">
				<Typography variant="h6">
					Main information
				</Typography>
			</AppBar>
			<TextField label="Enter your first name"
			           onChange={handleChange("firstName")}
			           defaultValue={firstName}/>
			<br/>
			<TextField label="Enter your last name"
			           onChange={handleChange("lastName")}
			           defaultValue={lastName}/>
			<br/>
			<TextField label="Enter your email"
			           onChange={handleChange("email")}
			           defaultValue={email}/>
			<br/>

			<Button variant="contained"
			        color="primary"
			        style={styles.button}
			        onClick={nextStep}>
				Continue
			</Button>
		</ThemeProvider>);
}

export default FormUserDetails;