import { teal } from "@material-ui/core/colors";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";


export default createMuiTheme({
	palette: {
		primary: {
			main: teal[800],
		},
	},
});